const headerButton = document.querySelector(".header__menu-controls");
const headerMenu = document.querySelector(".header__menu");
const burgerButton = document.querySelector(".header__menu-icon.header__menu-icon--burger");
const crossButton = document.querySelector(".header__menu-icon.header__menu-icon--cross");
let displayHeaderMenu = false;

headerButton.addEventListener("click", function() {
    if (!displayHeaderMenu) {
        headerMenu.classList.add("header__menu--show");
        burgerButton.classList.add("header__menu-icon--hide")
        crossButton.classList.remove("header__menu-icon--hide")
        displayHeaderMenu = true;
    } else {
        headerMenu.classList.remove("header__menu--show");
        burgerButton.classList.remove("header__menu-icon--hide")
        crossButton.classList.add("header__menu-icon--hide")
        displayHeaderMenu = false;
    }
});